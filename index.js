const express = require('express');
const mongoose = require('mongoose');
const app = express();
const port = 80;

app.get('/', (req, res) => res.send('<h1>Hello Siemens!</h1>'));

app.listen(port, () => {
	console.log('Express Server Up! port ' + port);
});
